# encoding: utf-8
from io import BytesIO
import requests
from bs4 import BeautifulSoup

vinicio = 14084
vfim = 14140
nome_arquivo = "One Piece DUBLADO"

with open(str(nome_arquivo)+".m3u","w") as o:
  o.write('#EXTM3U' + '\n \n')
  o.write("################# One Piece DUBLADO #################" + '\n \n')

  while (vinicio <= vfim):
    url = requests.get('http://www.animesonlinebr.com.br/video/' + str(vinicio))
    soup = BeautifulSoup(url.content, 'html.parser')
    page_link = soup.find(itemprop='embedURL')
    page_episodio = soup.find(itemprop='keywords')
    page_episodio = page_episodio.get('content')
    if 'One Piece' in page_episodio:
      page_episodio = page_episodio.split("- One Piece Dublado")
      page_episodio = page_episodio[1].split(' online,')
      #page_episodio = page_episodio[1].replace('Dublado ','')
      #page_episodio = page_episodio.replace('online','')

      print(page_episodio[0])
      o.write('#EXTINF:-1 tvg-logo="http://goo.gl/VqEvBa" group-title="One Piece DUBLADO",')
      o.write(page_episodio[0]+'\n')
      o.write(page_link.get('href') + '\n \n')
    vinicio = vinicio + 1