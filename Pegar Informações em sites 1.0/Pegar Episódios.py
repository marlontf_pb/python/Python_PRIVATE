# encoding: utf-8
import os
import requests
from bs4 import BeautifulSoup
from time import sleep
import re

vinicio = 650
vfim = 790
nome_arquivo = "One Piece DUBLADO"

o = open(str(nome_arquivo)+'.m3u','wb')
o.write("#EXTM3U" + '\n \n')
o.write("################# One Piece DUBLADO #################" + '\n \n')

url2 = requests.get('https://one-piece-x.com.br/guia-de-episodios/')
soup2 = BeautifulSoup(url2.content, 'html.parser')
nome_episodio = soup2.find(id='guia')

while (vinicio <= vfim):
	url = requests.get('https://www.animeaionline.net/one-piece-' + str(vinicio))
	soup = BeautifulSoup(url.content, 'html.parser')
	page_link = soup.find('video')
	if 'mp4' in str(soup):
		if 'autobuffer' in str(page_link):
			page_link = page_link.get('src')
		else:
			page_link = soup.find(type='video/mp4')
			page_link = page_link.get('src')
				#page_episodio = page_episodio.split("-")
				#page_episodio = page_episodio[1].split(',')
				#page_episodio = page_episodio[0].split('!')
				#page_episodio = page_episodio[0].replace('Dublado ','')
				#page_episodio = page_episodio.encode('utf8')
				
		if vinicio < 100:
			vbusca = '#0' + str(vinicio)
		elif vinicio <10:
			vbusca = '#00' + str(vinicio)
		else:
			vbusca = '#' + str(vinicio)

		start = str(nome_episodio).find(vbusca) + 13
		end = str(nome_episodio).find('</h2>', start)
		
		print('Episódio ' + str(vinicio) + ' - ' + str(nome_episodio)[start:end])
		o.write('#EXTINF:-1 tvg-logo="http://goo.gl/VqEvBa" group-title="One Piece DUBLADO",')
		o.write('Episódio ' + str(vinicio) + ' - ' + str(nome_episodio)[start:end] + '\n')
		o.write(page_link + '\n \n')

	else:
		o.write('\n \n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n \n')
		
	vinicio = vinicio + 1

o.close()