# Exemplo de scrap de página de anime e conversão para m3u (IPTV)

# encoding: utf-8
import os
import requests
from bs4 import BeautifulSoup
from time import sleep
import re

vinicio = 14084
vfim = 14213
nome_arquivo = "One Piece DUBLADO"

o = open(str(nome_arquivo)+'.m3u','wb')
o.write("#EXTM3U" + '\n \n')
o.write("################# One Piece DUBLADO #################" + '\n \n')

while (vinicio <= vfim):
  url = requests.get('http://www.animesonlinebr.com.br/video/' + str(vinicio))
  soup = BeautifulSoup(url.content, 'html.parser')
  page_link = soup.find(itemprop='embedURL')
  page_episodio = soup.find(itemprop='keywords')
  page_episodio = page_episodio.get('content')
  page_episodio = page_episodio.split("-")
  page_episodio = page_episodio[1].split(',')
  page_episodio = page_episodio[0].split('!')
  page_episodio = page_episodio[0].replace('Dublado ','')
  page_episodio = page_episodio.encode('utf8')
  
  print(page_episodio)
  o.write('#EXTINF:-1 tvg-logo="http://goo.gl/VqEvBa" group-title="One Piece DUBLADO",')
  o.write(page_episodio+'\n')
  o.write(page_link.get('href') + '\n \n')
  vinicio = vinicio + 1


o.close()